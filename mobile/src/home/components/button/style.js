import styled from 'styled-components';
import { View, Text } from 'react-native';

export const ConnectContainer = styled(View)`
  padding: 10px;
  border-radius: 10px;
  background-color: #b8b8ff;
`;
export const ConnectTitle = styled(Text)`
  color: #fff;
`;
